//
//  NoteViewController.swift
//  Notes
//
//  Created by Konrad on 01/06/2018.
//  Copyright © 2018 Konrad. All rights reserved.
//

import Foundation

import UIKit

class NoteDetailsViewController: UIViewController {
    
    @IBOutlet weak var noteText: UITextView!
    
    var afterSaveProtocol : AfterSaveProtocol?
    var note: NoteItem?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        noteText.text = (note?.title)! + "\r\n" + (note?.description)!
        if let fontSize = NumberFormatter().number(from: (note?.fontSize.description)!) {
            noteText.font = UIFont.init(name: (note?.fontFamilyName)!, size: CGFloat(truncating: fontSize))
        }
    }
    
    @IBAction func redirectToViewEditNote(_ sender: UIButton) {
        performSegue(withIdentifier: "editNote", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? NoteViewController {
            destination.note = note
            destination.afterSaveProtocol = afterSaveProtocol
        }
    }
    
}
