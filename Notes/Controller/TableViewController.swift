//
//  ViewController.swift
//  Notes
//
//  Created by Konrad on 29/05/2018.
//  Copyright © 2018 Konrad. All rights reserved.
//

import UIKit


class TableViewController: UITableViewController, AfterSaveProtocol {
    
    private let webSerivceNote = WebServiceNote()
    private let cellIdentifier = "cell"
    private var noteItems: [NoteItem] = []
    
    fileprivate func fillTable(with page: (Array<NoteItem>)) {
        noteItems = page
        self.title = "My Notes"
        DispatchQueue.main.async {
            self.tableView.reloadData()
            self.tableView.dataSource = self;
            self.tableView.delegate = self;
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let nibName = String(describing: NoteCell.self)
        let nib = UINib(nibName: nibName, bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "noteCellIdentifier")
        
        loadTable()
    }
    
    func loadTable() {
        webSerivceNote.getNote(completion: {page in
            DispatchQueue.main.async {
                self.fillTable(with: page)
            }
        })
    }
    
    @IBAction func addNewNote(_ sender: Any) {
        performSegue(withIdentifier: "note", sender: self)
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCellEditingStyle.delete {
            let  noteId = String(noteItems[indexPath.row].id!)
            noteItems.remove(at: indexPath.row)
            webSerivceNote.deleteNote(noteId: noteId)
            tableView.reloadData()
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return noteItems.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "noteCellIdentifier", for: indexPath) as! NoteCell
        cell.titleLabel?.text = noteItems[indexPath.row].title
        cell.dateLabel?.text = noteItems[indexPath.row].createdDate?.description
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "showDetails", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? NoteDetailsViewController {
            destination.note = noteItems[(tableView.indexPathForSelectedRow?.row)!]
            destination.afterSaveProtocol = self
        } else if let destination = segue.destination as? NoteViewController {
            destination.note = NoteItem(title: "", description: "", fontFamilyName: "Arial", fontSize: 10)
            destination.afterSaveProtocol = self
        }
    }
    
    func afterSaveAction() {
        loadTable()
    }
}





