//
//  NoteViewController.swift
//  Notes
//
//  Created by Konrad on 01/06/2018.
//  Copyright © 2018 Konrad. All rights reserved.
//

import Foundation

import UIKit

class NoteViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    
    @IBAction func showFonts(_ sender: Any) {
        fontPicker.isHidden = !fontPicker.isHidden
    }
    
    @IBOutlet weak var noteText: UITextView!
    
    @IBOutlet weak var fontPicker: UIPickerView!
    
    @IBOutlet weak var fontSize: UITextField!
    var note: NoteItem?
    
    var afterSaveProtocol: AfterSaveProtocol?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        fontPicker.delegate = self
        fontPicker.dataSource = self
        fontPicker.isHidden = true
        fontSize.delegate = self
        
        if(note != nil) {
           noteText.text =  (note?.title)! + "\r\n" + (note?.description)!
            if let fontSize = NumberFormatter().number(from: (note?.fontSize.description)!) {
                noteText.font = UIFont.init(name: (note?.fontFamilyName)!, size: CGFloat(truncating: fontSize))
                self.fontSize.text = Int((note?.fontSize)!).description
                fontPicker.selectRow(UIFont.familyNames.index(of: (note?.fontFamilyName)!)!, inComponent: 0, animated: true)
            }
            
        }
        
    }
    
    @IBAction func saveNote(_ sender: Any) {
        assignValueToNote()
        let webSerivceNote = WebServiceNote()
        note?.fontSize = Float(CGFloat((noteText.font?.pointSize)!))
        note?.fontFamilyName = (noteText.font?.familyName)!
        webSerivceNote.saveNote(note: note!, afterSave: afterSaveProtocol!)
        navigationController?.popToRootViewController(animated: true)
    }
    
    func assignValueToNote() {
        var text = noteText.text.components(separatedBy: "\n")
        if(text.count > 1) {
            note?.title = text.first!
        }
        if(text.count > 2) {
            text.remove(at: 0)
            note?.description = text.joined(separator: "\n")
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return UIFont.familyNames.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return UIFont.familyNames[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        noteText.font = UIFont.init(name: UIFont.familyNames[row], size: (noteText.font?.pointSize)!)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let fontSize = NumberFormatter().number(from: fontSize.text!) {
            noteText.font = UIFont.init(name: (noteText.font?.familyName)!, size: CGFloat(truncating: fontSize))
        }
    }
}
