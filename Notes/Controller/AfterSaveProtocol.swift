//
//  AfterSaveProtocol.swift
//  Notes
//
//  Created by Konrad on 03/06/2018.
//  Copyright © 2018 Konrad. All rights reserved.
//

import Foundation

import UIKit

protocol AfterSaveProtocol {
    func afterSaveAction()
}
