//
//  WebServiceNote
//  Notes
//
//  Created by Konrad on 30/05/2018.
//  Copyright © 2018 Konrad. All rights reserved.
//

import Foundation
import UIKit

typealias PageCompletion = (Array<NoteItem>) -> Void


struct WebServiceNote {
    
    private var host = "mynotekonradzik.herokuapp.com"
    private var notesPath = "/note/"
    
    
    func getNote(completion: @escaping PageCompletion) {
        guard let url = getFindComponents(findPath: "find").url else {
            return
        }
        execute(request: URLRequest(url: url), with: completion)
    }
    
    func deleteNote(noteId: String) {
        let urlComponents = getFindComponents(findPath: "delete")
        var request = URLRequest(url: urlComponents.url!)
        request.httpMethod = "DELETE"
        request.allHTTPHeaderFields = ["Content-Type": "application/json"]
        let task = URLSession.shared.dataTask(with: request as URLRequest)
        task.resume()
    }
    
    func saveNote(note: NoteItem, afterSave: AfterSaveProtocol) {
        print(note)
        let urlComponents = getFindComponents(findPath: "add")
        do {
            var request = URLRequest(url: urlComponents.url!)
            request.httpMethod = "POST"
            request.allHTTPHeaderFields = ["Content-Type": "application/json"]
            request.httpBody =  try JSONEncoder().encode(note)
            URLSession.shared.dataTask(with: request, completionHandler: {(_data, _response, _error) in
                afterSave.afterSaveAction()
            }).resume()
        } catch {}
    }
    

    private func getFindComponents(findPath: String) -> URLComponents {
        var urlComponents = URLComponents()
        urlComponents.scheme = "http"
        urlComponents.host = host
        urlComponents.path = notesPath.appending(findPath)
        
        return urlComponents
    }
    
    private func execute(request: URLRequest, with completion: @escaping PageCompletion) {
        URLSession.shared.dataTask(with: request, completionHandler: {(_data, _response, _error) in
            if let data = _data {
                do {
                    let notePage = try JSONDecoder().decode(Array<NoteItem>.self, from: data)
                    completion(notePage)
                } catch {
                    print("Error xD")
                }
            }
        }).resume()
    }
}

