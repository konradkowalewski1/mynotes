//
//  NoteItem.swift
//  Notes
//
//  Created by Konrad on 30/05/2018.
//  Copyright © 2018 Konrad. All rights reserved.
//

import Foundation

import UIKit

struct NoteItem: Codable {
    var id: Int?
    var title: String
    var description: String
    var createdDate: String?
    var fontSize: Float
    var fontFamilyName: String

    init(title: String, description: String, fontFamilyName: String, fontSize: Float) {
        self.title = title;
        self.description = description;
        self.fontSize = fontSize;
        self.fontFamilyName = fontFamilyName;
    }
}
